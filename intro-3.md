# Обзор облачных провайдеров

Крупнейшие облачные провайдеры:
- [Amazon Web Services (AWS)](https://aws.amazon.com) — самый крупный
- [Microsoft Azure](https://azure.microsoft.com) — продвигает гибридные облачные решения
- [Google Cloud](https://cloud.google.com) — не работает в Китае
- [Alibaba](https://www.alibabacloud.com) — самый крупный в Китае
- [Яндекс.Облако](https://cloud.yandex.ru) — позволяет размещать данные в пределах РФ

Часто облачных провайдеров сравнивают с провайдерами VPS, поскольку спектр их услуг пересекаются.

VPS — Virtual Private Server (виртуальный частный сервер). Провайдер VPS — это компания, которая предоставляет VPS в аренду.

Популярные провайдеры VPS:
- [DigitalOcean](https://digitalocean.com)
- [Linode](https://www.linode.com)
- [Vultr](https://www.vultr.com)

Основное преимущество провайдеров VPS: более низкая цена при сравнимых характеристиках.

Недостатки провайдеров VPS: небольшое количество услуг, чуть меньшая надежность и плохая масштабируемость.

Литература:

- https://www.canalys.com/static/press_release/2020/Canalys---Cloud-market-share-Q4-2019-and-full-year-2019.pdf
- https://www.zdnet.com/article/the-top-cloud-providers-of-2020-aws-microsoft-azure-google-cloud-hybrid-saas/
