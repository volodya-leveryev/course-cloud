# Типы облачных услуг

**Public Cloud** — подход при котором данные и вычисления находятся на серверах облачного провайдера.

Достоинства:
- низкая стоимость
- не нужно обслуживать
- масштабируемость
- надежность

Недостаток:
- сложно обеспечить безопасность важных данных

**Private Cloud** — подход при котором данные и вычисления находятся на частных серверах.  
Синоними: on-premise, on-prem, self-hosted.

Достоинства:
- гибкость
- безопасность

Недостаток:
- высокая стоимость

**Hybrid Cloud** — комбинированный подход, который совмещает достоинства обоих подходов. Клиент сам решает где и какую часть данных хранить, где и сколько вычислений проводить. Основной недостаток — дороговизна и сложность реализации.

[![Видео](https://img.youtube.com/vi/mYeMYqn2ARE/mqdefault.jpg)](https://www.youtube.com/watch?v=mYeMYqn2ARE)

Литература:
- https://www.bmc.com/blogs/public-private-hybrid-cloud/
- https://www.redhat.com/en/topics/cloud-computing/public-cloud-vs-private-cloud-and-hybrid-cloud
