# Введение

[Типы облачных услуг](./intro-1.md)

[Типы облачных услуг](./intro-2.md)

Крупнейшие облачные провайдеры:
- Amazon Web Services (AWS) — самый крупный
- Microsoft Azure — продвигает гибридные облачные решения
- Google Cloud — не работает в Китае
- Alibaba — самый крупный в Китае
- Яндекс.Облако — позволяет размещать данные в пределах РФ

У большинства облачных провайдеров есть набор бесплатных услуг — Free tier.

SLA — Service Level Agreement — соглашение по которому облачный провайдер обязуется обеспечить бесперебойную работу приложения и сохранность данных. В SLA оговаривается срок возможной неработоспособности приложения. При нарушении оговоренных лимитов пользователь, как правило, получает значительные скидки.

Облачные провайдеры размещают свои серверы в дата-центрах.  
Зоны доступности — состоит как-минимум из 2 дата-центров.  
Регионы — состоит из нескольких зон доступности.

https://www.cloudping.info/  
https://www.cloudping.co/grid  
https://cloudpingtest.com/  

Вендор-лок — облачные провайдеры стремятся привязать пользователей к своей платформе:
- специфичные API (ранний подход);
- удобство размещения и анализа данных (новый тренд).

Новая тенденция: Multi-Cloud — тренд на создание приложений, не зависящих (не подверженных vendor-lock) от облачного провайдера. В частности, этот подход базируется на активном использовании контейнеров и микросервисов облегчающих переход между провайдерами.

Провайдеры, которые специализируются на IaaS:
- DigitalOcean
- Linode
- Vultr
- SSD Nodes

Провайдеры, которые специализируются на Multi-cloud и гибридных облачных решениях:
- IBM
- Dell/VMWare
- Hewlett Packard Enterprise

Провайдеры, которые специализируются на PaaS и SaaS:
- Salesforce
- SAP
- Oracle
- Adobe

Литература:
- https://aws.amazon.com/ru/free/
- https://azure.microsoft.com/ru-ru/free/
- https://cloud.google.com/free
- https://medium.com/@jaychapel/aws-vs-azure-vs-google-cloud-market-share-2020-what-the-latest-data-shows-9afd4accf8d7
- https://www.zdnet.com/article/the-top-cloud-providers-of-2020-aws-microsoft-azure-google-cloud-hybrid-saas/
