# Типы облачных услуг

Классификация по уровню абстракции:
- **Infrastructure as a Service (IaaS)** — клиент платит за аренду инфраструктуры. Например: аренда серверов.
- **Platform as a Service (PaaS)** — клиент платит за использование программной платформы. Например: Google App Engine.
- **Software as a Service (SaaS)** — клиент платит за использование программного продукта. Например: Google Drive.

Другие связанные термины:
- **Data as a Service (DaaS)** — доступ к данным как сервис.
- **Database as a service (DBaaS)** — доступ к базе данных как сервис.
- **Function as a Service (FaaS)** — запуск функций как сервис.
- **... as a Service (XaaS)** — что-то предоставляемое как услуга (сервис).

[![Видео](https://img.youtube.com/vi/0eiaqlB0vic/mqdefault.jpg)](https://www.youtube.com/watch?v=0eiaqlB0vic)

Литература:
- https://www.ispsystem.ru/news/xaas
- https://brainhub.eu/blog/cloud-architecture-saas-faas-xaas/
